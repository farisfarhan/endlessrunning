﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class tenenet : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "53d36abc194586e954785327c54bd180";

    private string alias = "faris";
    private string fname = "faris";
    private string lname = "farhan";
    private string id = "1234";

    private string alias2 = "robocop";
    private string fname2 = "robo";
    private string lname2 = "cop";
    private string id2 = "1234";

    private string alias3 = "robot";
    private string fname3 = "fe";
    private string lname3 = "as";
    private string id3 = "1234";

    private string alias4 = "robot1";
    private string fname4 = "fe";
    private string lname4 = "as2";
    private string id4 = "1234";

    private string metric_id = "metric3";
    private string metric_name = "metric3";
    private string metric_type = "point";
    private string value = "0";

    // Start is called before the first frame update
    void Start()
    {
         //StartCoroutine(RegisterNewPlayer());
        StartCoroutine(getLeaderboard());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator RegisterNewPlayer()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/createPlayer" + "?token=" + token +
            "&alias=" + alias4 + "&id=" + id4 + "&fname=" + fname4 + "&lname=" + lname4);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }
        
    }

    private IEnumerator GetPlayer()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token +"&alias=" + alias2 );

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }


    }
    private IEnumerator insertPlayerActivity()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + alias4 + "&id=" + metric_id + "&operator=" + "add" + "&value=" + 100);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }


    }
    private IEnumerator getLeaderboard()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token + "&id=" + "topscorer");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
        }


    }
}
