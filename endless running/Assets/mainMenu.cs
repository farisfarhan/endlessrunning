﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class mainMenu : MonoBehaviour
{
    public Text username;
    void Start()
    {
        username.text = login.alias;
    }
    public void startGame()
    {

        SceneManager.LoadScene("Save1");

    }
    public void leaderboard()
    {

        SceneManager.LoadScene("Leaderboard");

    }
    public void changeAccount()
    {

        SceneManager.LoadScene("Login");

    }
    public void exitGame()
    {

        Application.Quit();

    }

}
