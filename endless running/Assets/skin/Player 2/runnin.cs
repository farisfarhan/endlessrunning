﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class runnin : MonoBehaviour
{
    private string laneChange = "n";
    private string jump = "n";
    private string form;
    private string message;
    private string status;
    public static Vector3 playerPosition;
    public static string collide = "n";
    public static int score = 0;
    public static int lastScore = 0;
    public GameObject bg;

    const string URI = "http://api.tenenet.net";
    const string token = "bfa3fbbae0c6c152d9e7c3df82e3ab17";

    public static Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        // GetComponent<Animator>().Play("Quick Roll To Run");
        anim = GetComponent<Animator>();
        anim.SetBool("laneChanging", false);
        anim.SetBool("isGameLost", false);
        anim.SetBool("isJumping", false);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 5);
        StartCoroutine(countScore());
    }

    // Update is called once per frame
    void Update()
    {
        

        playerPosition = transform.position;
        if (Input.GetKey("a") && laneChange == "n" && transform.position.x > -.9 && jump == "n")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(-1, 0, 5);
            laneChange = "y";
            anim.SetBool("laneChanging", true);
            StartCoroutine(stopLaneCh());
        }
        if (Input.GetKey("d") && laneChange == "n" && transform.position.x < .9 && jump=="n")
        {
            GetComponent<Rigidbody>().velocity = new Vector3(1, 0, 5);
            laneChange = "y";
            anim.SetBool("laneChanging", true);
            StartCoroutine(stopLaneCh());
        }
        if (Input.GetKey("space") && jump =="n" && laneChange == "n" )
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 1.5f, 5);
            jump = "y";
            anim.SetBool("isJumping", true);
            StartCoroutine(stopJump());
        }
      
    }
    IEnumerator stopJump()
    {
        yield return new WaitForSeconds(.5f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, -1.5f, 5);
        yield return new WaitForSeconds(.5f);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 5);

        jump = "n";
        anim.SetBool("isJumping", false);

    }

    IEnumerator stopLaneCh()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 5);
        anim.SetBool("laneChanging", false);
        laneChange = "n";

        //Debug.Log(GetComponent<Transform>().position);
    }

    IEnumerator countScore()
    {
        yield return new WaitForSeconds(0.2f);
        if(collide == "n")
        {
            score += 10;
            StartCoroutine(countScore());
        }

    }


    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "obstacle")
        {
            //Debug.Log("OUCH!");
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            lastScore = score;
            StartCoroutine(setPlayerScore(lastScore));
            collide = "y";
            score = 0;
            anim.SetBool("isGameLost", true);
            bg.SetActive(true);
            StopAllCoroutines();
            

        }
    }

    IEnumerator setPlayerScore(int score)
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/insertPlayerActivity" + "?token=" + token + "&alias=" + login.alias + "&id=" + register.metricID + "&operator=" + "add" + "&value=" + score);

        yield return www.SendWebRequest();



        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            form = www.downloadHandler.text;
            Debug.Log("New score set!");
        }
    }
}
