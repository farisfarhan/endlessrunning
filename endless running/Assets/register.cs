﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class register : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "bfa3fbbae0c6c152d9e7c3df82e3ab17";
    private string alias;
    private string fname;
    private string lname;
    private string id;

    public static string metricID = "metric02";

    public Text usernameInput;
    public Text fnameInput;
    public Text lnameInput;
    public Text pwInput;

    public void registerUser()
    {
        StartCoroutine(takeUsername());
    }

    IEnumerator takeUsername()
    {
        alias = usernameInput.text;
        fname = fnameInput.text;
        lname = lnameInput.text;
        id = pwInput.text;
        Debug.Log(alias);
        Debug.Log(fname);
        Debug.Log(lname);
        Debug.Log(id);

        UnityWebRequest www = UnityWebRequest.Get(URI + "/createPlayer" + "?token=" + token +
            "&alias=" + alias + "&id=" + id + "&fname=" + fname + "&lname=" + lname);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            //Debug.Log(www.downloadHandler.text);
            Debug.Log("RegisterSuccessful");
            SceneManager.LoadScene("Login");
        }
    }

    public void loginPage()
    {
        SceneManager.LoadScene("Login");
    }

}
