﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class displayScore : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "bfa3fbbae0c6c152d9e7c3df82e3ab17";
    private string leaderboardId = "leaderboard01";
    private string form;
    private string message;
    private string status;
    private string tempValue;
    public static string[] player;
    public static string[] ranking;
    public static string[] score;
    public static string hello;
    public static int currentRank;

    public Transform image;
    public Transform scorePlace;
    private Transform scorePlace2;
    public GameObject contentRow;
    public GameObject rank;
    public GameObject username;
    public GameObject highscore;
    private Vector3 nextTileSpawn;
    private Vector3 nextUsernameSpawn;
    private Vector3 nextRankSpawn;
    private Vector3 nextScoreSpawn;



    //public Text displayedRank;
    //public Text displayedUsername;

    private List<string> playerList = new List<string>();
    private List<string> rankingList = new List<string>();
    private List<string> scoreList = new List<string>();

    public List<Text> usernameList = new List<Text>(); 

    private int rankNum = 1;
    private int playerNameNum = 0;

    public static bool isScoreAssigned = false;
    public static bool isCurrentRankAssigned = false;

    void Start()
    {
        StartCoroutine(insertLeaderboardData());
        player = playerList.ToArray();
        //Debug.Log(player[0]);
        Debug.Log("katsini");

        nextTileSpawn.x = nextTileSpawn.x + 962;
        nextTileSpawn.y = nextTileSpawn.y + 520;

        nextUsernameSpawn = nextTileSpawn;
        nextUsernameSpawn.y = nextUsernameSpawn.y - 10;
        nextUsernameSpawn.x = nextUsernameSpawn.x - 80;

        nextRankSpawn = nextTileSpawn;
        nextRankSpawn.y = nextRankSpawn.y - 0;
        nextRankSpawn.x = nextRankSpawn.x - 320;

        nextScoreSpawn = nextTileSpawn;
        nextScoreSpawn.y = nextScoreSpawn.y - 0;
        nextScoreSpawn.x = nextScoreSpawn.x + 230;

        //nextScoreSpawn = scorePlace.transform.position;

    }
    void Update()
    {
        if (isScoreAssigned)
        {

            StartCoroutine(spawnScore());
            isScoreAssigned = false;
        }
           
    }

    public void mainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    IEnumerator insertLeaderboardData()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getLeaderboard" + "?token=" + token +
            "&id=" + leaderboardId);

        yield return www.SendWebRequest();

        

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            form = www.downloadHandler.text;
            string[] extractData = form.Split(new char[] { '"' }, System.StringSplitOptions.RemoveEmptyEntries);
           
            for (int i = 0; i < extractData.Length; i++)
            {
                if (extractData[i] == "message")
                    message = extractData[i + 2];
                if (extractData[i] == "status")
                    status = extractData[i + 2];
                if (extractData[i] == "alias")
                {
                    playerList.Add(extractData[i + 2]);
                }
                    
                if (extractData[i] == "rank")
                {
                    rankingList.Add(rankNum.ToString());
                    rankNum += 1;
                }    
            }

            

            if (status == "0")
                Debug.Log("leaderboard failed to be loaded");
            else
            {
                player = playerList.ToArray();
                ranking = rankingList.ToArray();
                Debug.Log("Successfully retrieved leaderboard data.");
                
                isScoreAssigned = true;
            }

            Debug.Log(playerList);
            
        }
        yield return new WaitForSeconds(0.2f);
        //hello = player[0];
    }

    

    IEnumerator spawnScore()
    {
        yield return new WaitForSeconds(0.2f);
        for (int count = 0; count < player.Length; count ++)
        {
            UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token +
           "&alias=" + player[count]);

            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                form = www.downloadHandler.text;
                Debug.Log(form);
                string[] extractData = form.Split(new char[] { '"' }, System.StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < extractData.Length; i++)
                {
                    if (extractData[i] == "message")
                        message = extractData[i + 2];
                    if (extractData[i] == "status")
                        status = extractData[i + 2];
                    if (extractData[i] == "value")
                    {
                        scoreList.Add(extractData[i + 2]);
                    }
                }
                if (status == "0")
                    Debug.Log("player failed to be loaded");
                else
                {
                    score = scoreList.ToArray();
                    Debug.Log("Successfully retrieved player score.");
                }
            }
        }
        for (int count = 0; count < player.Length; count++)
        {
           

                rank.GetComponent<Text>().text = ranking[count];
            username.GetComponent<Text>().text = player[count];
            highscore.GetComponent<Text>().text = score[count];

            Instantiate(contentRow, nextTileSpawn, contentRow.transform.rotation, scorePlace);
            Instantiate(username, nextUsernameSpawn, username.transform.rotation, scorePlace);
            Instantiate(rank, nextRankSpawn, rank.transform.rotation, scorePlace);
            Instantiate(highscore, nextScoreSpawn, highscore.transform.rotation, scorePlace);


            nextTileSpawn.y = nextTileSpawn.y - 43;
            //nextUsernameSpawn = nextTileSpawn;
            nextRankSpawn.y = nextRankSpawn.y - 43;
            nextUsernameSpawn.y = nextUsernameSpawn.y - 43;
            nextScoreSpawn.y = nextScoreSpawn.y - 43;

            //yield return new WaitForSeconds(0.2f);
        }
        

    }
}
