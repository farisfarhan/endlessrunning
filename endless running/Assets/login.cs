﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class login : MonoBehaviour
{
    const string URI = "http://api.tenenet.net";
    const string token = "bfa3fbbae0c6c152d9e7c3df82e3ab17";
    public static string alias;
    private string form;
    private string message;
    private string status;
    public Text usernameInput;
    public Text error;

    IEnumerator loginWithUsername()
    {
        UnityWebRequest www = UnityWebRequest.Get(URI + "/getPlayer" + "?token=" + token + "&alias=" + alias);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            form = www.downloadHandler.text;
           string[] extractData = form.Split(new char[] { '"' }, System.StringSplitOptions.RemoveEmptyEntries);

            for (int i=0; i<extractData.Length; i++)
            {
                if (extractData[i] == "message")
                    message = extractData[i + 2];
                if (extractData[i] == "status")
                    status = extractData[i + 2];
            }
            if (status == "0")
            {
                Debug.Log("Player does not exist");
                error.text = "Player does not exist, please register.";
            }
                
            else
            {
                Debug.Log("LoginSuccessful");
                SceneManager.LoadScene("MainMenu");
            }
        }
    }
    public void takeUsername()
    {
        alias = usernameInput.text;
        StartCoroutine(loginWithUsername());
    }

    public void registerPage()
    {

        SceneManager.LoadScene("register");

    }
}
